package com.company.test;

import com.company.Trit;

import java.util.Arrays;
import java.util.List;

public class BasicTest {


    public final static List<Trit> TRITS_LIST = Arrays.asList(Trit.TRUE, Trit.MAYBE, Trit.FALSE);

    public final static List<String> OPERATORS = Arrays.asList("NOT", "AND", "OR", "EQUALS", "XOR");

    public final static List<String> UNARY_OPERATORS = Arrays.asList("NOT");


    public static Trit getExpectedResult(Trit a, String operation, Trit b) {


        /** TODO:
         *   A     B          a & b ; AND
         * ───── ─────        ───────────
         * false false  ────► false
         * false  true  ────► false
         * false maybe  ────► maybe
         *  true false  ────► false
         *  true  true  ────►  true
         *  true maybe  ────► maybe
         * maybe false  ────► maybe
         * maybe  true  ────► maybe
         * maybe maybe  ────► maybe
         *
         *   A     B          a | b ; OR
         * ───── ─────        ──────────
         * false false  ────► false
         * false  true  ────►  true
         * false maybe  ────► maybe
         *  true false  ────►  true
         *  true  true  ────►  true
         *  true maybe  ────►  true
         * maybe false  ────► maybe
         * maybe  true  ────►  true
         * maybe maybe  ────► maybe
         *
         *   A     B          a ^ b ; XOR
         * ───── ─────        ───────────
         * false false  ────► false
         * false  true  ────►  true
         * false maybe  ────► maybe
         *  true false  ────►  true
         *  true  true  ────► false
         *  true maybe  ────► maybe
         * maybe false  ────► maybe
         * maybe  true  ────► maybe
         * maybe maybe  ────► maybe
         *
         *   A     B          a ! b ; NOR
         * ───── ─────        ───────────
         * false false  ────►  true
         * false  true  ────► false
         * false maybe  ────► maybe
         *  true false  ────► false
         *  true  true  ────► false
         *  true maybe  ────► maybe
         * maybe false  ────► maybe
         * maybe  true  ────► maybe
         * maybe maybe  ────► maybe
         *
         *   A     B          a ¡ b ; NAND
         * ───── ─────        ────────────
         * false false  ────►  true
         * false  true  ────►  true
         * false maybe  ────►  true
         *  true false  ────►  true
         *  true  true  ────► false
         *  true maybe  ────► maybe
         * maybe false  ────►  true
         * maybe  true  ────► maybe
         * maybe maybe  ────► maybe
         *
         *   A     B          a xnor b ; XNOR
         * ───── ─────        ───────────────
         * false false  ────►  true
         * false  true  ────► false
         * false maybe  ────► maybe
         *  true false  ────► false
         *  true  true  ────►  true
         *  true maybe  ────► maybe
         * maybe false  ────► maybe
         * maybe  true  ────► maybe
         * maybe maybe  ────► maybe
         *
         */
        switch (operation) {
            case "AND":
                if (a.is(Trit.TRUE) && b.is(Trit.TRUE)) {
                    return Trit.TRUE;
                } else if (a.is(Trit.TRUE) && b.is(Trit.MAYBE)) {
                    return Trit.MAYBE;
                } else if (a.is(Trit.TRUE) && b.is(Trit.FALSE)) {
                    return Trit.FALSE;
                } else if (a.is(Trit.MAYBE) && b.is(Trit.TRUE)) {
                    return Trit.MAYBE;
                } else if (a.is(Trit.MAYBE) && b.is(Trit.MAYBE)) {
                    return Trit.MAYBE;
                } else if (a.is(Trit.MAYBE) && b.is(Trit.FALSE)) {
                    return Trit.FALSE;
                } else if (a.is(Trit.FALSE) && b.is(Trit.TRUE)) {
                    return Trit.FALSE;
                } else if (a.is(Trit.FALSE) && b.is(Trit.MAYBE)) {
                    return Trit.FALSE;
                } else if (a.is(Trit.FALSE) && b.is(Trit.FALSE)) {
                    return Trit.FALSE;
                }
            case "OR":
                if (a.is(Trit.TRUE) && b.is(Trit.TRUE)) {
                    return Trit.TRUE;
                } else if (a.is(Trit.TRUE) && b.is(Trit.MAYBE)) {
                    return Trit.TRUE;
                } else if (a.is(Trit.TRUE) && b.is(Trit.FALSE)) {
                    return Trit.TRUE;
                } else if (a.is(Trit.MAYBE) && b.is(Trit.TRUE)) {
                    return Trit.TRUE;
                } else if (a.is(Trit.MAYBE) && b.is(Trit.MAYBE)) {
                    return Trit.MAYBE;
                } else if (a.is(Trit.MAYBE) && b.is(Trit.FALSE)) {
                    return Trit.MAYBE;
                } else if (a.is(Trit.FALSE) && b.is(Trit.TRUE)) {
                    return Trit.TRUE;
                } else if (a.is(Trit.FALSE) && b.is(Trit.MAYBE)) {
                    return Trit.MAYBE;
                } else if (a.is(Trit.FALSE) && b.is(Trit.FALSE)) {
                    return Trit.FALSE;
                }
            case "EQUALS":
                if (a.is(Trit.TRUE) && b.is(Trit.TRUE)) {
                    return Trit.TRUE;
                } else if (a.is(Trit.TRUE) && b.is(Trit.MAYBE)) {
                    return Trit.MAYBE;
                } else if (a.is(Trit.TRUE) && b.is(Trit.FALSE)) {
                    return Trit.FALSE;
                } else if (a.is(Trit.MAYBE) && b.is(Trit.TRUE)) {
                    return Trit.MAYBE;
                } else if (a.is(Trit.MAYBE) && b.is(Trit.MAYBE)) {
                    return Trit.MAYBE;
                } else if (a.is(Trit.MAYBE) && b.is(Trit.FALSE)) {
                    return Trit.MAYBE;
                } else if (a.is(Trit.FALSE) && b.is(Trit.TRUE)) {
                    return Trit.FALSE;
                } else if (a.is(Trit.FALSE) && b.is(Trit.MAYBE)) {
                    return Trit.MAYBE;
                } else if (a.is(Trit.FALSE) && b.is(Trit.FALSE)) {
                    return Trit.TRUE;
                }
            case "XOR":
                if (a.is(Trit.TRUE) && b.is(Trit.TRUE)) {
                    return Trit.FALSE;
                } else if (a.is(Trit.TRUE) && b.is(Trit.MAYBE)) {
                    return Trit.MAYBE;
                } else if (a.is(Trit.TRUE) && b.is(Trit.FALSE)) {
                    return Trit.TRUE;
                } else if (a.is(Trit.MAYBE) && b.is(Trit.TRUE)) {
                    return Trit.MAYBE;
                } else if (a.is(Trit.MAYBE) && b.is(Trit.MAYBE)) {
                    return Trit.MAYBE;
                } else if (a.is(Trit.MAYBE) && b.is(Trit.FALSE)) {
                    return Trit.MAYBE;
                } else if (a.is(Trit.FALSE) && b.is(Trit.TRUE)) {
                    return Trit.TRUE;
                } else if (a.is(Trit.FALSE) && b.is(Trit.MAYBE)) {
                    return Trit.MAYBE;
                } else if (a.is(Trit.FALSE) && b.is(Trit.FALSE)) {
                    return Trit.FALSE;
                }
            case "NOT":
                if (a.is(Trit.TRUE)) {
                    return Trit.FALSE;
                } else if (a.is(Trit.MAYBE)) {
                    return Trit.MAYBE;
                } else if (a.is(Trit.FALSE)) {
                    return Trit.TRUE;
                }
        }
        return null;
    }

    public static void testOperators() {

        for (String operator : OPERATORS) {

            boolean isUnaryOp = isUnaryOperator(operator);

            System.out.println("\n--- TRUTH TABLE FOR OPERATOR '" + operator + "' ---");

            for (int i = 0; i < TRITS_LIST.size(); ++i) {
                for (int j = 0; j < TRITS_LIST.size(); ++j) {

                    Trit a = TRITS_LIST.get(i);
                    Trit b = isUnaryOp ? null : TRITS_LIST.get(j);

                    Trit result = getResult(a, operator, b);


                    if (result.is(getExpectedResult(a, operator, b))) {
                        printOperation(a, operator, b, result);
                    } else {
                        throw new RuntimeException();
                    }

                    if (isUnaryOperator(operator)) {
                        // don't iterate over second parameter for unary iterators
                        break;
                    }
                }

            }
        }
    }

    private static boolean isUnaryOperator(String operator) {
        return UNARY_OPERATORS.contains(operator);
    }

    private static Trit getResult(Trit a, String operator, Trit b) {
        switch (operator) {
            case "AND":
                return Trit.logicalAnd(a, b);
            case "OR":
                return Trit.logicalOr(a, b);
            case "EQUALS":
                return Trit.logicalEquals(a, b);
            case "XOR":
                return Trit.logicalXor(a, b);
            case "NOT":
                return Trit.logicalNot(a);
            default:
                return null;
        }
    }

    private static void printOperation(Trit a, String operator, Trit b, Trit result) {

        if (!isUnaryOperator(operator)) {
            System.out.println(a.toString() + " " +  operator + " " + b.toString() + " = " + result.toString());
        } else {
            System.out.println(operator + " " + a.toString()  + " = " + result.toString());
        }

    }
}
