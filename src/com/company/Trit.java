package com.company;

import java.text.ParseException;
import java.util.Objects;

public class Trit implements java.io.Serializable, Comparable<Trit> {

    private enum TritEnum {
        TRUE("True"), MAYBE("Maybe"), FALSE("False");

        private final String name;

        private TritEnum(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }
    }

    public static final Trit TRUE = new Trit(TritEnum.TRUE);
    public static final Trit MAYBE = new Trit(TritEnum.MAYBE);
    public static final Trit FALSE = new Trit(TritEnum.FALSE);

    private static final long serialVersionUID = 7771518707977111035L;

    private final TritEnum value;

    public Trit(boolean value) {
        this.value = value ? TritEnum.TRUE : TritEnum.FALSE;
    }

    public Trit(TritEnum value) {
        this.value = value;

    }

    public static Trit parseTrit(String s) throws ParseException {
        if ("true".equalsIgnoreCase(s)) {
            return TRUE;
        } else if ("false".equalsIgnoreCase(s)) {
            return FALSE;
        } else if ("maybe".equalsIgnoreCase(s)) {
            return MAYBE;
        } else {
            throw new ParseException("Not valid trit value string representation", 0);
        }
    }

    public static Trit valueOf(String s) throws ParseException {
        return parseTrit(s);
    }

    public String toString() {
        return this.value.getName();
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o != null) {

            Trit result = null;

            if (getClass() == o.getClass()) {
                result = logicalEquals(o);

            } else if (Boolean.class == o.getClass()) {
                result = logicalEquals(o);
            }

            if (result != null) {
                if (result.is(TRUE)) {
                    return true;
                } else if (result.is(FALSE)) {
                    return false;
                } else if (result.is(MAYBE)) {
                    throw new RuntimeException();
                }
            }
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }

    @Override
    public int compareTo(Trit b) {
        return compare(this.value, b.value);

    }

    public boolean is(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Trit trit = (Trit) o;
        return value == trit.value;
    }

    public static int compare(Trit a, Trit b) {
        return compare(a.value, b.value);
    }

    public Trit logicalEquals(Object obj) {
        if (obj instanceof Trit) {
            return logicalEquals((Trit)obj);
        } if (obj instanceof  Boolean) {
            return logicalEquals(new Trit((Boolean)obj));
        }
        return Trit.FALSE;
    }


    public Trit logicalEquals(Trit other) {
        return logicalEquals(this, other);
    }

    public static Trit logicalEquals(Trit a, Trit b) {
        if(a == Trit.TRUE){
            return b;
        }else if(a == Trit.MAYBE){
            return Trit.MAYBE;
        }else{
            return logicalNot(b);
        }
    }

    public static Trit logicalAnd(Trit a, Trit b) {
        if(a == Trit.TRUE){
            return b;
        }else if(a == Trit.MAYBE){
            return (b == Trit.FALSE) ? Trit.FALSE : Trit.MAYBE;
        }else{
            return Trit.FALSE;
        }
    }

    public static Trit logicalOr(Trit a, Trit b) {
        if (a == Trit.TRUE) {
            return Trit.TRUE;
        } else if (a == Trit.MAYBE) {
            return (b == Trit.TRUE) ? Trit.TRUE : Trit.MAYBE;
        } else {
            return b;
        }
    }

    public static Trit logicalNot(Trit a) {
        if (a == Trit.TRUE) {
            return Trit.FALSE;
        } else if (a == Trit.MAYBE) {
            return Trit.MAYBE;
        } else {
            return Trit.TRUE;
        }
    }

    public static Trit logicalNand(Trit a, Trit b) {
        return logicalNot(logicalAnd(a, b));
    }

    public static Trit logicalNor(Trit a, Trit b) {
        return logicalNot((logicalOr(a, b)));
    }

    public static Trit logicalXor(Trit a, Trit b) {
        return logicalOr(logicalAnd(a, logicalNot(b)), logicalAnd(logicalNot(a), b));
    }


    public Trit logicalIf(Trit other) { // If 'this', then 'other'
        if(this == Trit.TRUE){
            return other;
        }else if(this == Trit.MAYBE){
            return (other == Trit.TRUE) ? Trit.TRUE : Trit.MAYBE;
        }else{
            return Trit.TRUE;
        }
    }

    // Private methods with TritEnum that is out of scope outside this class
    private static int compare(TritEnum x, TritEnum y) {
        return (x == y) ? 0 :
                (x.equals(TritEnum.TRUE) ? 1 :
                        (x.equals(TritEnum.FALSE) ? -1 :
                                (x.equals(TritEnum.MAYBE) && (y.equals(TritEnum.FALSE)) ? 1 :
                                        -1)));
    }

    private static TritEnum logicalIfAthenB(TritEnum a, TritEnum b){
        if(a == TritEnum.TRUE){
            return b;
        }else if(a == TritEnum.MAYBE){
            return (b == TritEnum.TRUE) ? TritEnum.TRUE : TritEnum.MAYBE;
        }else{
            return TritEnum.TRUE;
        }
    }

    private TritEnum TritValue() {
        return value;
    }

    private static Trit valueOf(TritEnum t) throws ParseException {
        return new Trit(t);
    }
}
